// jshint esversion: 6

const Sequelize = require('sequelize');
const config = require('./config.json');

const sequelize = new Sequelize(config.database, config.dbUser, config.dbPassword, {
  host: config.host,
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },

  logging: false
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection to database has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const User = sequelize.define('user', {
  user: {
    type:Sequelize.STRING
  },
  badges: {
    type: Sequelize.JSON,
    defaultValue: {
      deadhead: false,
      doublewing: false,
      powerplant: false,
      trihard: false,
      omnicient: false,
      steelheart: false,
      gaydragon: false
    }
  }
});

module.exports = {
  users: User,
  database: sequelize
};
