// jshint esversion: 6

let token = 'MzcxODMxODE4NDAxNDgwNzA0.DM7W5A._n9RCTgGQXK80b8BRabglDaB_AE';
let invite = 'https://discordapp.com/oauth2/authorize?client_id=371831818401480704&scope=bot&permissions=52224';

const Eris = require('eris');

var GIFEncoder = require('gifencoder');
var Canvas = require('canvas');
var fs = require('fs');
var path = require('path');

let badges = ['skull', 'wing', 'skull', 'wing', 'skull', 'wing', 'skull', 'wing', 'skull', 'wing', 'skull', 'wing', 'skull', 'wing', 'skull', 'wing', 'skull', 'wing', 'skull', 'wing', 'skull', 'wing'];


var bot = new Eris(token);
bot.on("ready", () => {
    console.log("Ready!");
});
bot.on("messageCreate", (msg) => {
    if(msg.content === "!ping") {
        bot.createMessage(msg.channel.id, "Pong!");
    }
    if(msg.content === "!inventory") {
        let inventory = getBadges();
        bot.createMessage(msg.channel.id, "", {
            file: inventory.out.getData(),
            name: 'inventory.gif'
        }).catch((err) => {
          bot.createMessage(msg.channel.id, "D: I can't embed here!");
        });
    }
});


// use node-canvas

function getBadges() {
    let inventory = new GIFEncoder(32*(badges.length%6), 32*Math.floor(badges.length/6));

    inventory.start();
    inventory.setRepeat(0);   // 0 for repeat, -1 for no-repeat
    inventory.setDelay(100);  // frame delay in ms
    inventory.setQuality(10); // image quality. 10 is default.
    for(let frameNumber = 0; frameNumber < 7; frameNumber++) {
        let frameCanvas = new Canvas(32*(badges.length%6), 32*Math.floor(badges.length/6));
        let frame = frameCanvas.getContext('2d');
        frame.fillStyle = '#36393e';
        frame.fillRect(0, 0, 32*(badges.length%6), 32*Math.floor(badges.length/6));
        for(let badge = 0; badge < badges.length; badge++) {
            let badgeImage = new Canvas.Image();
            badgeImage.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', badges[badge], `${frameNumber}.png`));
            frame.drawImage(badgeImage, 32*(badge%6), 32*Math.floor(badge/6));
        }
        inventory.addFrame(frame);
    }

    inventory.finish();
    return inventory;
}
bot.connect();