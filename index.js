// jshint esversion: 6

let invite = 'https://discordapp.com/oauth2/authorize?client_id=371831818401480704&scope=bot&permissions=52224';

const Eris = require('eris');
const config = require('./config');

const db = require('./sequelize');

var GIFEncoder = require('gifencoder');
var Canvas = require('canvas');
var fs = require('fs');
var path = require('path');

var bot = new Eris(config.token);
bot.on("ready", () => {
    console.log("Connected to Discord.");
    db.users.sync();
});

bot.on("messageCreate", (msg) => {
    if(msg.content === "!ping") {
        bot.createMessage(msg.channel.id, "Pong!");
    }
    if(msg.content === "!inventory" || msg.content === "!inv") {
        db.users.findAll({
            where: {
                user: msg.author.id
            }
        })
        .then(users => {
            if(users.length >= 1) {
                let inventory = getBadges(users[0].badges);
                bot.createMessage(msg.channel.id, "", {
                    file: inventory.out.getData(),
                    name: 'inventory.gif'
                }).catch((err) => {
                  bot.createMessage(msg.channel.id, "D: I can't embed here!");
                });
            }
            else {
                bot.createMessage(msg.channel.id, "You are not in the database! Use `!init` to add yourself to it.");
            }
        });
    }
    if(msg.content === "!init") {
        db.users.findAll({
            where: {
                user: msg.author.id
            }
        })
        .then(users => {
            if(users.length >= 1) {
                bot.createMessage(msg.channel.id, "You are already in the database! Use `!inventory` to see your badges.");
            }
            else {
                bot.createMessage(msg.channel.id, "Adding you to the database...")
                .then(m => {
                    db.users.create({
                        user: msg.author.id
                    })
                    .then(() => {
                        m.edit('Added you to the database!');
                    });
                });
            }
        });
    }
    if(msg.content.startsWith("!award")) {
        if(!config.admins.includes(msg.author.id)) {
            bot.createMessage(msg.channel.id, "Sorry, but I can't let you do that.")
        }
        else {
            let args = msg.content.match(/\S+/g) || [];
            if (!config.badges.includes(args[2].toLowerCase())) {
                bot.createMessage(msg.channel.id, `${args[2]} is not a valid badge! D:`);
                return;
            }
            db.users.findAll({
                where: {
                    user: msg.mentions[0].id
                }
            })
            .then(users => {
                if(users.length >= 1) {
                    let badges = users[0].badges;
                    badges[args[2]] = true;
                    bot.createMessage(msg.channel.id, "Awarding the badge...")
                    .then(m => {
                        db.users.update({
                            badges: badges,
                        }, {
                            where: {
                            user: msg.mentions[0].id
                            }
                        })
                        .then(() => {
                            m.edit('Awarded the badge to ' + msg.mentions[0].username + '!');
                        });
                    });
                }
                else {
                    bot.createMessage(msg.channel.id, "That user is not in the database! They have to use `!init` to themselves to it.");
                }
            });
        }
    }
    if(msg.content.startsWith("!revoke")) {
        if(!config.admins.includes(msg.author.id)) {
            bot.createMessage(msg.channel.id, "Sorry, but I can't let you do that.")
        }
        else {
            let args = msg.content.match(/\S+/g) || [];
            if(!config.badges.includes(args[2].toLowerCase())) {
                bot.createMessage(msg.channel.id, `${args[2]} is not a valid badge! D:`);
                return;
            }
            db.users.findAll({
                where: {
                    user: msg.mentions[0].id
                }
            })
            .then(users => {
                if(users.length >= 1) {
                    let badges = users[0].badges;
                    badges[args[2]] = false;
                    bot.createMessage(msg.channel.id, "Revoking the badge...")
                    .then(m => {
                        db.users.update({
                            badges: badges,
                        }, {
                            where: {
                            user: msg.mentions[0].id
                            }
                        })
                        .then(() => {
                            m.edit('Revoked the badge from ' + msg.mentions[0].username + '!');
                        });
                    });
                }
                else {
                    bot.createMessage(msg.channel.id, "That user is not in the database! They have to use `!init` to themselves to it.");
                }
            });
        }
    }
    if (msg.content.startsWith('!eval')) {
        if (msg.author.id === '250322741406859265') {
          let toEval = msg.content.substr(msg.content.indexOf(' ') + 1)
          toEval = toEval.replace(/^(\n*```.*\n)*/, '')
          toEval = toEval.replace(/^(\n*`)/, '')
          toEval = toEval.replace(/(```)$/, '')
          toEval = toEval.replace(/(`)$/, '')
          console.log(toEval)
          try {
            bot.createMessage(msg.channel.id, `Input\n\`\`\`js\n${toEval}\n\`\`\`\nOutput\n\`\`\`js\n${eval(toEval)}\n\`\`\``) // eslint-disable-line no-eval
          } catch (err) {
            bot.createMessage(msg.channel.id, `Error\n\`\`\`js\n${err.stack}\`\`\``)
          }
        }
      }
});


// use node-canvas

function getBadges(badges) {
    let inventory = new GIFEncoder(192, 64);

    inventory.start(badges);
    inventory.setRepeat(0);   // 0 for repeat, -1 for no-repeat
    inventory.setDelay(100);  // frame delay in ms
    inventory.setQuality(4); // image quality. 10 is default.
    for(let frameNumber = 0; frameNumber < 7; frameNumber++) {
        let frameCanvas = new Canvas(192, 64);
        let frame = frameCanvas.getContext('2d');
        let background = new Canvas.Image();
        background.src = fs.readFileSync(path.join(__dirname, 'images', `inventory-bg.png`));
        // Draw the shadows, will re-enable when I have the shadows done.
        frame.drawImage(background, 0, 0);
        // Draw Badges
        if(badges.deadhead) {
            let deadheadBadge = new Canvas.Image();
            deadheadBadge.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'deadhead', `${frameNumber}.png`));
            frame.drawImage(deadheadBadge, 0, 0);
        }
        else {
            let shadow = new Canvas.Image();
            shadow.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'deadhead', `shadow.png`));
            frame.drawImage(shadow, 0, 0);
        }
        if(badges.doublewing) {
            let doublewingBadge = new Canvas.Image();
            doublewingBadge.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'doublewing', `${frameNumber}.png`));
            frame.drawImage(doublewingBadge, 35, 0);
        }
        else {
            let shadow = new Canvas.Image();
            shadow.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'doublewing', `shadow.png`));
            frame.drawImage(shadow, 35, 0);
        }
        if(badges.powerplant) {
            let powerplantBadge = new Canvas.Image();
            powerplantBadge.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'powerplant', `${frameNumber}.png`));
            frame.drawImage(powerplantBadge, 70, 0);
        }
        else {
            let shadow = new Canvas.Image();
            shadow.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'powerplant', `shadow.png`));
            frame.drawImage(shadow, 70, 0);
        }
        if(badges.trihard) {
            let trihardBadge = new Canvas.Image();
            trihardBadge.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'trihard', `${frameNumber}.png`));
            frame.drawImage(trihardBadge, 96, 0);
        }
        else {
            let shadow = new Canvas.Image();
            shadow.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'trihard', `shadow.png`));
            frame.drawImage(shadow, 96, 0);
        }
        if(badges.omniscient) {
            let omniscientBadge = new Canvas.Image();
            omniscientBadge.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'omniscient', `${frameNumber}.png`));
            frame.drawImage(omniscientBadge, 12, 32);
        }
        else {
            let shadow = new Canvas.Image();
            shadow.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'omniscient', `shadow.png`));
            frame.drawImage(shadow, 12, 32);
        }
        if(badges.steelheart) {
            let steelheartBadge = new Canvas.Image();
            steelheartBadge.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'steelheart', `${frameNumber}.png`));
            frame.drawImage(steelheartBadge, 46, 32);
        }
        else {
            let shadow = new Canvas.Image();
            shadow.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'steelheart', `shadow.png`));
            frame.drawImage(shadow, 46, 32);
        }
        if(badges.gaydragon) {
            let gaydragonBadge = new Canvas.Image();
            gaydragonBadge.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'gaydragon', `${frameNumber}.png`));
            frame.drawImage(gaydragonBadge, 80, 32);
        }
        else {
            let shadow = new Canvas.Image();
            shadow.src = fs.readFileSync(path.join(__dirname, 'images', 'frames', 'gaydragon', `shadow.png`));
            frame.drawImage(shadow, 80, 32);
        }
        inventory.addFrame(frame);
    }

    inventory.finish();
    return inventory;
}
bot.connect();